﻿using Nos3.Common;
using System;
using System.Net;
using System.Net.Sockets;

namespace Nos3.Server
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var options = Options.ConfigureOptions<ServerOptions>(args, "server.json");
            var tcp = new TcpListener(IPAddress.Any, options.Port);
            Console.WriteLine($"NOS3 chat server at " + tcp.LocalEndpoint);
            tcp.Start();
            using (var cancellationSource = Utils.OnCancelKeyPress())
            using (cancellationSource.Token.Register(() => tcp.Stop()))
            {
                var server = new ChatServer();
                server.Run(cancellationSource.Token);
                while (!cancellationSource.IsCancellationRequested)
                {
                    server.AcceptClient(tcp.AcceptTcpClient());
                }
            }
        }
    }
}
