﻿using Nos3.Common;

namespace Nos3.Server
{
    internal class ServerOptions
    {
        [PositiveInt]
        public int Port { get; set; }
    }
}
