﻿using Nos3.Common;
using System;
using System.Collections.Concurrent;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace Nos3.Server
{
    internal class ChatServer
    {
        private static ConcurrentDictionary<string, TcpClient> _users = new ConcurrentDictionary<string, TcpClient>();
        private static Channel<Message> _channel = Channel.CreateUnbounded<Message>();

        public async void Run(CancellationToken cancellationToken)
        {
            await Task.Yield();
            while (!cancellationToken.IsCancellationRequested)
            {
                if (await _channel.Reader.ReadAsync(cancellationToken) is Message message)
                {
                    if (message.Addressee == null)
                    {
                        foreach (var user in _users)
                        {
                            await WriteAsync(user.Value.GetStream(), string.Empty,
                                $"{message.Addresser} to everyone: {message.Text}");
                        }
                    }
                    else if (_users.TryGetValue(message.Addresser, out var addresser))
                    {
                        if (_users.TryGetValue(message.Addressee, out var addressee))
                        {
                            await WriteAsync(addresser.GetStream(), string.Empty,
                                $"You to {message.Addressee}: {message.Text}.");
                            await WriteAsync(addressee.GetStream(), "private",
                                $"{message.Addresser} to you: {message.Text}");
                        }
                        else
                        {
                            await WriteAsync(addresser.GetStream(), string.Empty,
                                $"Message delivery error: no user {message.Addressee}.");
                        }
                    }
                }
            }
        }

        public async void AcceptClient(TcpClient client)
        {
            var id = Guid.NewGuid();
            Console.WriteLine($"[{id}] Accepted connection from {client.Client.RemoteEndPoint}.");
            await Task.Yield();
            string nickname = null;
            try
            {
                var stream = client.GetStream();

                nickname = (await AcceptMessage(id.ToString(), stream)).Text;
                await SendUserList(stream);
                if (!_users.TryAdd(nickname, client))
                {
                    await WriteAsync(stream, string.Empty, $"System message: nickname {nickname} is already taken.");
                    client.Dispose();
                    return;
                }

                Console.WriteLine($"[{id}] {client.Client.RemoteEndPoint} connected as {nickname}.");
                await WriteAsync(stream, string.Empty, $"Welcome to chat, {nickname}!");
                foreach (var st in _users)
                {
                    if (st.Key == nickname) continue;
                    await WriteAsync(st.Value.GetStream(), "joined", nickname);
                }
                while (await AcceptMessage(nickname, stream) is Message message)
                {
                    await _channel.Writer.WriteAsync(message);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"[{id}] Exception: {e}.");
            }
            client.Dispose();
            if (nickname != null)
            {
                _users.TryRemove(nickname, out _);
                foreach (var st in _users)
                {
                    await WriteAsync(st.Value.GetStream(), "leaved", nickname);
                }
            }
            Console.WriteLine($"[{id}] Disconnected.");
        }

        private static async Task SendUserList(NetworkStream stream)
        {
            await stream.WriteAsync(BitConverter.GetBytes(_users.Count));
            foreach (var user in _users)
            {
                var userBytes = Encoding.UTF8.GetBytes(user.Key);
                await stream.WriteAsync(BitConverter.GetBytes(userBytes.Length));
                await stream.WriteAsync(userBytes);
            }
        }

        private async Task<Message> AcceptMessage(string addresser, NetworkStream stream)
        {
            try
            {
                var header = await Utils.ReadAsync(stream, 8);
                var addressee = await Utils.ReadAsync(stream, BitConverter.ToInt32(header, 0));
                var text = await Utils.ReadAsync(stream, BitConverter.ToInt32(header, 4));
                return new Message(addresser, addressee.Length == 0 ? null : addressee, text);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        private async Task WriteAsync(Stream stream, string addresseeBytes, string textBytes)
        {
            var addressee = Encoding.UTF8.GetBytes(addresseeBytes);
            var text = Encoding.UTF8.GetBytes(textBytes);
            await stream.WriteAsync(BitConverter.GetBytes(addressee.Length));
            await stream.WriteAsync(BitConverter.GetBytes(text.Length));
            await stream.WriteAsync(addressee);
            await stream.WriteAsync(text);
        }
    }
}