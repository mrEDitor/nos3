﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;

namespace Nos3.Common
{
    public class Options
    {
        public static TOptions ConfigureOptions<TOptions>(string[] args, string optionsFilename)
            where TOptions : class, new()
        {
            var options = new TOptions();
            var configuration = new ConfigurationBuilder()
                .AddCommandLine(args)
                .AddJsonFile(optionsFilename, optional: true)
                .Build();
            configuration.Bind(options);
            var result = new DataAnnotationValidateOptions<TOptions>(null)
                .Validate(null, options);
            if (result.Failed)
            {
                throw new ArgumentException($"{result.FailureMessage}");
            }

            return options;
        }
    }
}
