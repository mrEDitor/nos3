﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Nos3.Common
{
    public class Utils
    {
        public static CancellationTokenSource OnCancelKeyPress()
        {
            var cancellationSource = new CancellationTokenSource();
            Console.CancelKeyPress += (s, a) => cancellationSource.Cancel();
            return cancellationSource;
        }

        public static async Task<byte[]> ReadAsync(Stream stream, int length, CancellationToken token = default)
        {
            var buffer = new byte[length];
            for (var read = 0; read < buffer.Length;)
            {
                var delta = await stream.ReadAsync(buffer, read, buffer.Length - read, token);
                read += delta;
                if (delta == 0 || token.IsCancellationRequested)
                {
                    return null;
                }
            }

            return buffer;
        }
    }
}
