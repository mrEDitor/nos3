﻿using System.Text;

namespace Nos3.Common
{
    public class Message
    {
        public Message(string text)
        {
            Text = text;
        }

        public Message(string addresser, byte[] addressee, byte[] text)
        {
            Addresser = addresser;
            if (addressee != null)
            {
                Addressee = Encoding.UTF8.GetString(addressee);
            }
            Text = Encoding.UTF8.GetString(text);
        }

        public string Addresser { get; set; }
        public string Addressee { get; set; }
        public string Text { get; set; }
    }
}
