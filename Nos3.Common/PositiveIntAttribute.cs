﻿using System.ComponentModel.DataAnnotations;

namespace Nos3.Common
{
    public class PositiveIntAttribute : ValidationAttribute
    {
        public PositiveIntAttribute()
        {
            ErrorMessage = "Value must be positive integer.";
        }

        public override bool IsValid(object value) =>
            value == null || (int)value > 0;
    }
}
