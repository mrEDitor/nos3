﻿using Nos3.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Nos3.WpfClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Action<string> _onSend;
        private Stream _stream;

        public MainWindow()
        {
            InitializeComponent();
            Users.Items.Add(new User());
            Messages.Items.Add($"Please enter a server address:port.");
            _onSend = DoConnect;
        }

        public async void DoConnect(string connectionString)
        {
            var ip = connectionString.Split(new[] { ':' }, 2);
            if (TryConnect(ip, out var client))
            {
                var cancellation = new CancellationTokenSource();
                Closing += (s, e) => cancellation.Cancel();
                _stream = client.GetStream();
                await PickNickname(connectionString);
                await GetUsers(cancellation);

                try
                {
                    while (!cancellation.IsCancellationRequested)
                    {
                        var header = await Utils.ReadAsync(_stream, 8, cancellation.Token);
                        var eventType = await Utils.ReadAsync(_stream, BitConverter.ToInt32(header, 0), cancellation.Token);
                        var textBytes = await Utils.ReadAsync(_stream, BitConverter.ToInt32(header, 4), cancellation.Token);
                        var text = Encoding.UTF8.GetString(textBytes);
                        var eventValue = Encoding.UTF8.GetString(eventType);
                        switch (eventValue)
                        {
                            case "joined":
                                Users.Items.Add(new User(text));
                                Messages.Items.Add($"{text} joined the chat.");
                                break;
                            case "leaved":
                                Users.Items.Remove(new User(text));
                                Messages.Items.Add($"{text} leaved the chat.");
                                break;
                            default:
                                Messages.Items.Add(text);
                                break;
                        }
                        if (eventValue.StartsWith("private:") && IsFriend(eventValue.Substring("private:".Length)))
                        {
                            using (var f = File.OpenWrite(Fix(eventValue.Substring("private:".Length) + ".pm.txt")))
                            {
                                var v = Encoding.UTF8.GetBytes(text + "\n");
                                f.Write(v, 0, v.Length);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    Messages.Items.Add("Server closed the connection.");
                    Messages.Items.Add($"Please enter a server address:port.");
                    Users.Items.Clear();
                    Users.Items.Add(new User());
                    _onSend = DoConnect;
                }
                client.Dispose();
            }
            else
            {
                Messages.Items.Add($"No server found on {connectionString}");
            }
        }

        private string Fix(string v)
        {
            string invalid = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());

            foreach (char c in invalid)
            {
                v = v.Replace(c.ToString(), "");
            }
            return v;
        }

        private bool IsFriend(string v)
        {
            return !string.IsNullOrEmpty(v) && File.ReadAllLines("friends.txt").Contains(v);
        }

        private async Task GetUsers(CancellationTokenSource cancellation)
        {
            var userCount = await Utils.ReadAsync(_stream, 4, cancellation.Token);
            for (int i = 0; i < BitConverter.ToInt32(userCount, 0); ++i)
            {
                var userNameLen = await Utils.ReadAsync(_stream, 4, cancellation.Token);
                byte[] userNameBytes = await Utils.ReadAsync(_stream, BitConverter.ToInt32(userNameLen, 0), cancellation.Token);
                Users.Items.Add(new User(Encoding.UTF8.GetString(userNameBytes)));
            }
        }

        private async Task PickNickname(string connectionString)
        {
            Messages.Items.Add($"Connected to {connectionString}. Please pick a nickname.");
            var nicknameSource = new TaskCompletionSource<string>();
            _onSend = nicknameSource.SetResult;
            var nickname = await nicknameSource.Task;
            await SendAsync(nickname);
            _onSend = s => SendAsync(s);
        }

        private bool TryConnect(string[] ip, out TcpClient client)
        {
            if (ip.Length == 2 && int.TryParse(ip[1], out var port))
            {
                client = new TcpClient(ip.First(), port);
                return true;
            }
            else
            {
                client = null;
                return false;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (Input.Text != string.Empty)
            {
                _onSend(Input.Text);
                Input.Text = string.Empty;
            }
        }

        private void TextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Button_Click(null, null);
            }
        }

        private async Task SendAsync(string text)
        {
            var addresseeBytes = Encoding.UTF8.GetBytes((Users.SelectedItem as User)?.Name ?? string.Empty);
            var textBytes = Encoding.UTF8.GetBytes(text);
            await _stream.WriteAsync(BitConverter.GetBytes(addresseeBytes.Length), 0, 4);
            await _stream.WriteAsync(BitConverter.GetBytes(textBytes.Length), 0, 4);
            await _stream.WriteAsync(addresseeBytes, 0, addresseeBytes.Length);
            await _stream.WriteAsync(textBytes, 0, textBytes.Length);

            string name = (Users.SelectedItem as User)?.Name;
            if (IsFriend(name))
            {
                try
                {
                    using (var f = File.OpenWrite(Fix(name + ".pm.txt")))
                    {
                        var v = Encoding.UTF8.GetBytes($"You to {name}: {text}\n");
                        f.Write(v, 0, v.Length);
                    }
                }
                catch (Exception e)
                {

                }
            }
        }

        private void Users_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var border = e.OriginalSource as FrameworkElement;
            if (!(border?.DataContext is User user) || user.Name == string.Empty)
            {
                return;
            }

            user.IsFriend = !user.IsFriend;
            Users.Items.Refresh();
            IEnumerable<string> friends = Array.Empty<string>();
            if (File.Exists("friends.txt"))
            {
                friends = File.ReadAllLines("friends.txt").Where(it => it != user.Name);
            }

            if (user.IsFriend)
            {
                friends = friends.Append(user.Name);
            }

            File.WriteAllLines("friends.txt", friends);
        }
    }
}
