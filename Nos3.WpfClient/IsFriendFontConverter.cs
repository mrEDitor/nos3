﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Nos3.WpfClient
{
    public class IsFriendFontConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value as bool? == true)
            {
                return FontWeights.Bold;
            }
            return FontWeights.Normal;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
