﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Nos3.WpfClient
{
    public class User
    {
        public User()
        {
            VisibleName = "<everyone>";
            Name = string.Empty;
        }

        public User(string name)
        {
            VisibleName = Name = name;
            try
            {
                IsFriend = File.ReadAllLines("friends.txt").Contains(name);
            }
            catch
            {
            }
        }

        public string VisibleName { get; private set; }

        public string Name { get; set; }

        public bool IsFriend { get; set; }

        public override bool Equals(object obj)
        {
            var user = obj as User;
            return user != null && Name == user.Name;
        }

        public override int GetHashCode()
        {
            return 539060726 + EqualityComparer<string>.Default.GetHashCode(Name);
        }
    }
}
